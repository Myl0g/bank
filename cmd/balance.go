package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/myl0g/bank/utils"
)

var noPiggyBanks bool

// balanceCmd represents the balance command
var balanceCmd = &cobra.Command{
	Use:     "balance [-a account]",
	Aliases: []string{"b"},
	Short:   "View your account balance",
	Long: `View the balance of one of your accounts.
	
	Example: "bank b -a Checking" would retrieve your Checking account balance.`,
	Run: func(cmd *cobra.Command, args []string) {
		transactions := utils.FetchAccount(account).Transactions
		balance := 0.0
		for _, transaction := range transactions {
			if transaction.ForPiggyBank && noPiggyBanks {
				continue
			}
			balance += transaction.Amount
		}
		fmt.Println(fmt.Sprintf("%.2f", balance))
	},
}

func init() {
	balanceCmd.Flags().BoolVar(&noPiggyBanks, "no-piggy-banks", false, "Exclude transactions made to piggy banks from the balance.")
	rootCmd.AddCommand(balanceCmd)
}
