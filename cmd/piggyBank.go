package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/myl0g/bank/classes"
	"gitlab.com/myl0g/bank/utils"
)

// piggyBankCmd represents the piggyBank command
var piggyBankCmd = &cobra.Command{
	Use:     "piggyBank [subcommand]",
	Aliases: []string{"p"},
	Short:   "Manage your piggy banks.",
	Long: `Piggy banks are virtual accounts. Money can be moved into them, and they start out with a goal amount.
	
	Avaliable subcommands are view, add, left, and delete.`,
}

var viewPiggyBanksCmd = &cobra.Command{
	Use:     "view",
	Aliases: []string{"v"},
	Short:   "View all your piggy banks",
	Run: func(cmd *cobra.Command, args []string) {
		for _, bank := range utils.GetAllPiggyBanks() {
			fmt.Println(bank.Name)
			fmt.Println("- Purpose: " + bank.Purpose)
			fmt.Print("- Balance: ")
			fmt.Println(bank.Amount)
			fmt.Print("- Goal: ")
			fmt.Println(bank.Goal)
		}
	},
}

var addPiggyBankCmd = &cobra.Command{
	Use:     "add [name] [purpose] [goal]",
	Aliases: []string{"a"},
	Short:   "Create a piggy bank",
	Long: `Create a new piggy bank with a name, purpose, and goal.
	
	Example: "bank p a Xbox ExamplePiggyBank 500" creates a new piggy bank with the name Xbox, purpose ExamplePiggyBank, and goal of 500.`,
	Run: func(cmd *cobra.Command, args []string) {
		goal, err := strconv.ParseFloat(args[2], 64)
		utils.Check(err)

		old := utils.GetAllPiggyBanks()
		new := append(old, classes.PiggyBank{Name: args[0], Purpose: args[1], Amount: 0.0, Goal: goal})
		utils.UpdateAllPiggyBanks(new)
	},
}

var leftPiggyBankCmd = &cobra.Command{
	Use:     "left [piggy bank name]",
	Aliases: []string{"l"},
	Short:   "See how close you are to your goal",
	Long: `See the distance from your piggy bank's balance to your goal.
	
	For example, if you have a piggy bank with a goal of 500 and a balance of 300, this command should spit out the number 200.`,
	Run: func(cmd *cobra.Command, args []string) {
		bank := utils.GetPiggyBank(args[0])
		fmt.Println(fmt.Sprintf("%.2f", bank.Goal-bank.Amount))
	},
}

var deletePiggyBankCmd = &cobra.Command{
	Use:     "delete [piggy bank name]",
	Aliases: []string{"d"},
	Short:   "Delete a piggy bank",
	Run: func(cmd *cobra.Command, args []string) {
		old := utils.GetAllPiggyBanks()
		toDelete := -1

		for index, bank := range old {
			if bank.Name == args[0] {
				toDelete = index
				break
			}
		}
		if toDelete == -1 {
			fmt.Println("No piggy bank with that name was found.")
			return
		}

		new := append(old[:toDelete], old[toDelete+1:]...)
		utils.UpdateAllPiggyBanks(new)
	},
}

func init() {
	piggyBankCmd.AddCommand(addPiggyBankCmd, viewPiggyBanksCmd, leftPiggyBankCmd, deletePiggyBankCmd)
	rootCmd.AddCommand(piggyBankCmd)
}
