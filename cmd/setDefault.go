package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/myl0g/bank/utils"
)

// setDefaultCmd represents the setDefault command
var setDefaultCmd = &cobra.Command{
	Use:   "setDefault",
	Short: "Set the default account, which is used when an account name is not specified",
	Long:  `Example: "bank setDefault Checking" allows you to use any command which takes the -a flag without specifying it (which will then assume you meant to use the Checking account).`,
	Run: func(cmd *cobra.Command, args []string) {
		config := utils.GetConfig()
		config.DefaultAccount = args[0]
		utils.UpdateConfig(config)
	},
}

func init() {
	rootCmd.AddCommand(setDefaultCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setDefaultCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setDefaultCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
