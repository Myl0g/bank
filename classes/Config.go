package classes

// Config represents the configuration file for the bank utility.
type Config struct {
	DefaultAccount string
}
