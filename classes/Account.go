package classes

// Account represents a bank account, with a balance, transaction history, and "piggy banks".
type Account struct {
	Name         string
	Transactions []Transaction
}
