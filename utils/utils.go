package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strings"

	"gitlab.com/myl0g/bank/classes"
)

// Check goes through the process of Checking an error for a non-nil value.
// It panics and prints a stacktrace if it detects one.
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

// GetConfigDir returns the path of bank's configuration directory.
// It can use the BANK_CONFIG_DIR environment variable or use the default of ~/.bank instead.
func GetConfigDir() string {
	if os.Getenv("BANK_CONFIG_DIR") != "" {
		return os.Getenv("BANK_CONFIG_DIR")
	}

	usr, err := user.Current()
	Check(err)

	return usr.HomeDir + "/.bank/"
}

// Initialize creates the filesystem directory structure for the bank to utilize.
// This should only run if the structure does not already exist!
func Initialize() {
	os.Mkdir(GetConfigDir(), 0700)
	UpdateConfig(classes.Config{DefaultAccount: ""})
	UpdateAllPiggyBanks([]classes.PiggyBank{})
}

// GetConfig retrieves the Config object from the configuration directory
func GetConfig() classes.Config {
	t := classes.Config{}
	err := json.Unmarshal([]byte(ReadFile(GetConfigDir()+"config.json")), &t)
	Check(err)
	return t
}

// UpdateConfig sets the config file in the configuration directory to match the given object.
func UpdateConfig(c classes.Config) {
	data, err := json.Marshal(c)
	Check(err)

	file, err := os.Create(GetConfigDir() + "config.json")
	Check(err)

	file.Write(data)
}

// GetAccountsDir returns the directory where account files are stored.
func GetAccountsDir() string {
	return GetConfigDir() + "accounts/"
}

// ReadFile returns a string with the contents of the provided file.
func ReadFile(path string) string {
	dat, err := ioutil.ReadFile(path)
	Check(err)
	return string(dat)
}

// FetchAccount finds the account with the given name and returns it as unmarshalled JSON.
func FetchAccount(name string) classes.Account {
	t := classes.Account{}

	if strings.Contains(name, ".json") {
		json.Unmarshal([]byte(ReadFile(GetAccountsDir()+name)), &t)
	} else {
		json.Unmarshal([]byte(ReadFile(GetAccountsDir()+name+".json")), &t)
	}
	return t
}

// FetchAllAccounts finds and returns a slice of every account present in the bank.
func FetchAllAccounts() []classes.Account {
	t := []classes.Account{}

	files, err := ioutil.ReadDir(GetAccountsDir())
	Check(err)

	for _, file := range files {
		t = append(t, FetchAccount(file.Name()))
	}

	return t
}

// UpdateAccount updates the given account by replacing the original version with the provided one.
func UpdateAccount(a classes.Account) {
	data, err := json.Marshal(a)
	Check(err)

	file, err := os.Create(GetAccountsDir() + a.Name + ".json")
	Check(err)

	file.Write(data)
}

// GetPiggyBank finds a piggy bank by name and returns it.
func GetPiggyBank(name string) classes.PiggyBank {
	for _, bank := range GetAllPiggyBanks() {
		if bank.Name == name {
			return bank
		}
	}
	return classes.PiggyBank{}
}

// GetAllPiggyBanks gets an array of piggy banks from the piggy bank file in the config directory.
func GetAllPiggyBanks() []classes.PiggyBank {
	t := []classes.PiggyBank{}
	err := json.Unmarshal([]byte(ReadFile(GetConfigDir()+"piggybanks.json")), &t)
	Check(err)
	return t
}

// UpdateAllPiggyBanks sets the piggy bank file in the config directory to match the given object.
func UpdateAllPiggyBanks(p []classes.PiggyBank) {
	data, err := json.Marshal(p)
	Check(err)

	file, err := os.Create(GetConfigDir() + "piggybanks.json")
	Check(err)

	file.Write(data)
}

// UpdatePiggyBank finds the old version of the given piggy bank and overwrites it.
func UpdatePiggyBank(p classes.PiggyBank) {
	all := GetAllPiggyBanks()
	index := -1

	for i, bank := range all {
		if bank.Name == p.Name {
			index = i
		}
	}
	if index == -1 {
		fmt.Println("No bank with that name found.")
		return
	}

	all[index] = p
	UpdateAllPiggyBanks(all)
}
